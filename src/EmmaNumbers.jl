module EmmaNumbers 

# ----------------------------------------------------
# --- Dependencies 
# ----------------------------------------------------
import Base.*
using Random
using PrettyTables
using Flux

# ----------------------------------------------------
# --- Exportation 
# ----------------------------------------------------
export EmmaNumber 
export @count_emma
export convert_network_emma

include("Flux2Emma.jl")

# ----------------------------------------------------
# --- Structure defined as per se 
# ----------------------------------------------------
struct EmmaNumber{T}
    val::T
end

# ----------------------------------------------------
# --- Define counter for operations
# ----------------------------------------------------
mutable struct EmmaCounter
    mul::Int 
    add::Int 
    muladd::Int 
    div::Int 
    pow::Int
end



# --- Create the global counter operator 
EmmaCounter() = EmmaCounter(0,0,0,0,0)
FLOPS = EmmaCounter()

""" Reset the counter. This function should be called before starting a new counting operation. By default called by @count_emma 
"""
function reset_count()
    global FLOPS = EmmaCounter()
end

function Base.show(io::IO, c::EmmaCounter)
    type_suffix = ["Occurence"]
    op_names    = [n for n ∈ fieldnames(EmmaCounter)]
    mat = [getfield(FLOPS, Symbol(name)) for name ∈ fieldnames(EmmaCounter)]
    pretty_table(io, mat;
                header = ["Occurence"],
                row_labels = op_names,
                newline_at_end = false)
end


""" 
Macro to reset the counter and start to count operations. Display the resulting operation
@count_emma my_func(param)
"""
macro count_emma(funcall) 
    count_emma(funcall)
end 
function count_emma(funccall)
    quote 
        # Ensure counter is re-initialized 
        reset_count()
        # Call the function 
        out = $(esc(funccall))
        # Display the performance summary 
        println(FLOPS)
        # Return the results to be used later
        out 
    end
end

# function Base.copy(c::EmmaCounter)
#     op_names    = [n for n ∈ fieldnames(EmmaCounter)]
#     t = tuple([getproperty(c,k) for k ∈ op_names]...)
#     return EmmaCounter(t...)
# end

# ----------------------------------------------------
# --- Type operations 
# ----------------------------------------------------
# conversions back from EmmaNumber
Base.Float64(x::EmmaNumber) = Float64(x.val)
Base.Float32(x::EmmaNumber) = Float32(x.val)
Base.Float16(x::EmmaNumber) = Float16(x.val)
Base.Int64(x::EmmaNumber) = Int64(x.val)
Base.Int32(x::EmmaNumber) = Int32(x.val)
Base.Int16(x::EmmaNumber) = Int16(x.val)
# Fallback to float
Core.AbstractFloat(x::EmmaNumber) = float(x.val)
Core.AbstractFloat(::Type{EmmaNumber{T}}) where {T}= T
# Base promotion rules 
Base.promote_rule(::Type{<:Integer},::Type{EmmaNumber{T}}) where {T} = EmmaNumber{T}
Base.promote_rule(::Type{Float64},::Type{EmmaNumber{T}}) where {T} = EmmaNumber{T}
Base.promote_rule(::Type{Float32},::Type{EmmaNumber{T}}) where {T} = EmmaNumber{T}
Base.promote_rule(::Type{Float16},::Type{EmmaNumber{T}}) where {T} = EmmaNumber{T}

Base.promote_rule(::Type{EmmaNumber{T}},::Type{EmmaNumber{R}}) where {T,R} = EmmaNumber{promote_rule(T,R)}

# ?? Don't get why ne need that as we have already promoting rules ?
# This creates an entry for convert(EmmaNumber,12) to constructor
Base.convert(::Type{EmmaNumber{R}},x::T) where {R<:Number,T<:Number} = EmmaNumber{promote_type(T,R)}(x)
Base.convert(::Type{T},x::EmmaNumber{T}) where {T<:Number} = x.val ## ADDED 


# ----------------------------------------------------
# --- Basic operators
# ----------------------------------------------------
# Imbalanced operator between T and EmmaNumber{T} with automatic promotion to EmmaNumber
import Base.+, Base.*, Base.-
(*)(x,y::EmmaNumber)  = (*)(EmmaNumber(x),y)
(*)(x::EmmaNumber,y)  = (*)(x,EmmaNumber(y))
(+)(x,y::EmmaNumber)  = (+)(EmmaNumber(x),y)
(+)(x::EmmaNumber,y)  = (+)(x,EmmaNumber(y))
(-)(x,y::EmmaNumber)  = (-)(EmmaNumber(x),y)
(-)(x::EmmaNumber,y)  = (-)(x,EmmaNumber(y))

# Define operators and increment counters 
for O in (:(+), :(-))
    @eval function Base.$O(x::EmmaNumber,y::EmmaNumber)
        global FLOPS.add += 1
        r = $O(x.val, y.val)
        return EmmaNumber(r)
    end
end
for O in (:(*),)
    @eval function Base.$O(x::EmmaNumber,y::EmmaNumber)
        global FLOPS.mul += 1
        r = $O(x.val, y.val)
        return EmmaNumber(r)
    end
end
for O in (:(/),)
    @eval function Base.$O(x::EmmaNumber,y::EmmaNumber)
        global FLOPS.div += 1
        r = $O(x.val, y.val)
        return EmmaNumber(r)
    end
end
for O in (:(^),)
    @eval function Base.$O(x::EmmaNumber,y::EmmaNumber)
        global FLOPS.pow += 1
        r = $O(x.val, y.val)
        return EmmaNumber(r)
    end
end
for O in (:(muladd),)
    @eval function Base.$O(x::EmmaNumber,y::EmmaNumber,z::EmmaNumber)
        global FLOPS.muladd += 1
        r = $O(x.val, y.val,z.val)
        return EmmaNumber(r)
    end
end


# ----------------------------------------------------
# --- Boolean operators 
# ----------------------------------------------------
for O in (:(+), :(-), :(*), :(/), :(^))
    @eval function Base.$O(x::EmmaNumber{T},y::Bool) where {T}
        r = $O(x.val, y)
        return EmmaNumber(r)
    end
end
for O in (:(+), :(-), :(*), :(/), :(^))
    @eval function Base.$O(y::Bool,x::EmmaNumber{T}) where {T}
        r = $O(x.val, y)
        return EmmaNumber(r)
    end
end
Bool(x::EmmaNumber{Bool}) = Bool(x.val)


# ----------------------------------------------------
# --- Not that basic operators but still
# ----------------------------------------------------
for O in (:(-), :(+),
          :sign,
          :prevfloat, :nextfloat,
          :round, :trunc, :ceil, :floor,
          :inv, :abs, :sqrt, :cbrt,
          :exp, :expm1, :exp2, :exp10,
          :log, :log1p, :log2, :log10,
          :rad2deg, :deg2rad, :mod2pi, :rem2pi,
          :sin, :cos, :tan, :csc, :sec, :cot,
          :asin, :acos, :atan, :acsc, :asec, :acot,
          :sinh, :cosh, :tanh, :csch, :sech, :coth,
          :asinh, :acosh, :atanh, :acsch, :asech, :acoth,
          :sinc, :sinpi, :cospi,
          :sind, :cosd, :tand, :cscd, :secd, :cotd,
          :asind, :acosd, :atand, :acscd, :asecd, :acotd,
         )
    @eval function Base.$O(x::EmmaNumber)
        r = $O(x.val)
        return EmmaNumber(r)
    end
end

for O in (:(-), :(+),
    :sigmoid, :sigmoid_fast, 
    :tanh, :tanh_fast, 
    :relu, :leakyrelu, :selu, :gelu, :rrelu
   )
    @eval function Flux.$O(x::EmmaNumber)
    r = $O(x.val)
    return EmmaNumber(r)
    end
end


# ----------------------------------------------------
# --- Comparisons operators
# ---------------------------------------------------- 
for O in (:(<), :(<=),:isless)
    @eval Base.$O(x::$EmmaNumber, y::$EmmaNumber) = $O(x.val, y.val)
end
for O in (:(<), :(<=),:isless)
    @eval Base.$O(x::$EmmaNumber, y::Number) = $O(x.val, y)
end
for O in (:(<), :(<=),:isless)
    @eval Base.$O(x::$Number, y::EmmaNumber) = $O(x, y.val)
end

# ----------------------------------------------------
# --- Tools, utilities 
# ----------------------------------------------------
# Opereators that are pure and type stable and that can be both apply on value AND on type
for O in (:typemin, :typemax,:zero,:length)
    @eval Base.$O(x::$EmmaNumber) = EmmaNumber($O(x.val))
    @eval Base.$O(::Type{EmmaNumber{T}}) where {T} = EmmaNumber($O(T))
end
# Operators that are not pure (as the one in base )
for O in (:isfinite,)
    @eval Base.$O(x::$EmmaNumber) = $O(x.val)
end


# ----------------------------------------------------
# --- Operators on vectors 
# ----------------------------------------------------
# Init from Base 
for O in (:zeros, :ones)
    @eval Base.$O(::Type{EmmaNumber{T}}, dims::Tuple{Int64}) where T = EmmaNumber.(Base.$O(T,dims))
end 
# Init from Random 
for O in (:rand, :randn)
    @eval Random.$O(::Type{EmmaNumber{T}}, dims::Tuple{Int64}) where T = EmmaNumber.(Random.$O(T,dims))
end 

@eval Base.round(::Type{T}, x::$EmmaNumber) where {T<:Integer} = round(T, x.val)
#@eval rem(x::$EmmaNumber, y::$EmmaNumber) = Core.Intrinsics.rem_float(x.val, y.val)




function *(A::AbstractMatrix{EmmaNumber{T}},B::AbstractVector{EmmaNumber{R}}) where {T,R}
    (M,N)   = size(A)
    M2 = length(B)
    @assert N == M2 "Impossible matrix multiplication ($(size(A))) x ($(size(B)))"
    U = promote_type(T,R)
    C = EmmaNumber.(zeros(T,M))
    for i ∈ axes(A,1)
            for k ∈ axes(A,2)
                if C[i] == 0 
                    # Prevent a non necessary accumulation here 
                    C[i] = A[i,k] * B[k]
                else
                    # Accumulation  
                    C[i] += A[i,k] * B[k]
                end
        end
    end
    return C
end

function *(A::AbstractMatrix{EmmaNumber{T}},B::AbstractMatrix{EmmaNumber{R}}) where {T,R}
    (M,N)   = size(A)
    (M2,N2) = size(B)
    @assert N == M2 "Impossible matrix multiplication ($(size(A))) x ($(size(B)))"
    U = promote_type(T,R)
    C = EmmaNumber.(zeros(T,M,N2))
    for i ∈ axes(A,1)
        for j ∈ axes(B,2)
            for k ∈ axes(A,2)
                if C[i,j] == 0 
                    # Prevent a non necessary accumulation here 
                    C[i,j] = A[i,k] * B[k,j]
                else
                    # Accumulation  
                    C[i,j] += A[i,k] * B[k,j]
                end
            end
        end
    end
    return C
end


end
