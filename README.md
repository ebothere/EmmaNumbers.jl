# EmmaNumbers.jl 


A small package to compute FLOPS of an operator.
It exports the macro `@count_emma` that can be used as 

```
N = 100
m = rand(N, N)
v = rand(N,N)
m = EmmaNumber.(m)
v = EmmaNumber.(v)
@count_emma m * v

┌────────┬───────────┐
│        │ Occurence │
├────────┼───────────┤
│    mul │     10000 │
│    add │     10000 │
│ muladd │         0 │
│    div │         0 │
│    pow │         0 │
└────────┴───────────┘

```